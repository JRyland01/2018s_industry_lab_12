package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {
    private Color color;
    private boolean test =false;
    public DynamicRectangleShape() {
        super();
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    @Override
    public void move(int width, int height){
        int x=   fDeltaX;
        int y= fDeltaY;
        super.move(width,height);
        if(x == -fDeltaX){
            color = Color.GREEN;
            test=false;
        }else if(y == -fDeltaY){
            test =true;
        }
    }

    @Override
    public void paint(Painter painter) {
        painter.setColor(Color.green);
        if(!test) {
            painter.setColor(color);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.black);
        }
        if(test){
            painter.setColor(Color.black);
            painter.drawRect(fX,fY,fWidth,fHeight);
        }
    }
}
