package ictgradschool.industry.bounce;

import java.awt.*;

public class CustomShape extends Shape {
    private boolean test = false;
    private Color color;
    public CustomShape() {
        super();
    }

    public CustomShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public CustomShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }
    @Override
    public void move(int width, int height){
        int x= fDeltaX;
        int y= fDeltaY;
        super.move(width,height);
        if(x == -fDeltaX){
            color = Color.GREEN;
            test=false;
        }else if(y == -fDeltaY){
            test =true;
        }
    }
    @Override
    public void paint(Painter painter) {
        Polygon polygon = new Polygon();
            polygon.addPoint(fX,fY+20);
            polygon.addPoint(20,fY);
            polygon.addPoint(fX+fWidth,fY);
            polygon.addPoint(fX+fWidth+20,fY+20);
            polygon.addPoint(fX+fWidth,fHeight);
            polygon.addPoint(fX+20,fY+fHeight);
            if (test) {
                painter.setColor(Color.orange);
                painter.drawPolygon(polygon);
                painter.setColor(Color.blue);
                painter.fillPolygon(polygon);
                painter.setColor(Color.black);
            }else if(!test){
                painter.setColor(Color.blue);
                painter.drawPolygon(polygon);
                painter.setColor(Color.orange);
                painter.fillPolygon(polygon);
                painter.setColor(Color.black);
            }
    }

}
