package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {

    public GemShape(){
        super();
    }
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }
    @Override
    public void paint(Painter painter) {
        Polygon polygon = new Polygon();
        if (fWidth>=40){
            polygon.addPoint(fX+0,fY+20);
            polygon.addPoint(fX+20,fY);
            polygon.addPoint(fX+fWidth,fY);
            polygon.addPoint(fX+fWidth+20,fY+20);
            polygon.addPoint(fX+fWidth,fY+fHeight);
            polygon.addPoint(fX+20,fY+fHeight);
        }else{
            polygon.addPoint(fX+0,fY+fHeight/2);
            polygon.addPoint(fX+fWidth/2,fY+0);
            polygon.addPoint(fX+fWidth,fY+fHeight/2);
            polygon.addPoint(fX+fWidth/2,fY+fHeight);
        }

        painter.drawPolygon(polygon);
    }
}
